<?php


namespace App\Models\Forms;


use App\Entity\Address;
use Symfony\Component\Validator\Constraints as Assert;

class AddressCreateForm
{
    /**
     * @var string $number
     * @Assert\NotNull()
     * @Assert\Regex("/\d+/")
     */
    private string $number;
    /**
     * @var string $street
     * @Assert\NotNull()
     */
    private string $street;
    /**
     * @var string $city
     * @Assert\NotNull()
     */
    private string $city;
    /**
     * @var string $country
     * @Assert\NotNull()
     */
    private string $country;

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return AddressCreateForm
     */
    public function setNumber(string $number): AddressCreateForm
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return AddressCreateForm
     */
    public function setStreet(string $street): AddressCreateForm
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return AddressCreateForm
     */
    public function setCity(string $city): AddressCreateForm
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return AddressCreateForm
     */
    public function setCountry(string $country): AddressCreateForm
    {
        $this->country = $country;
        return $this;
    }


    public static function toAddress(AddressCreateForm $form) {
        $address = new Address();

        $address->setNumber($form->number);
        $address->setStreet($form->street);
        $address->setCity($form->city);
        $address->setCountry($form->country);

        return $address;
    }
}