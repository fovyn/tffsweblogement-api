<?php


namespace App\Models\Forms;


use App\Entity\Logement;
use Symfony\Component\Validator\Constraints as Assert;

class LogementForm
{
    /**
     * @var LogementCreateForm $logement
     * @Assert\NotNull(message="Logement cannot be null")
     */
    public LogementCreateForm $logement;
    /**
     * @var AddressCreateForm $address
     */
    public AddressCreateForm $address;

    public static function toLogement(LogementForm $form) {
        /** @var Logement $logement */
        $logement = LogementCreateForm::toLogement($form->logement);
        $logement->setAddress(AddressCreateForm::toAddress($form->address));

        return $logement;
    }
}