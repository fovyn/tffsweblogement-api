<?php

namespace App\Models;

use App\Entity\Address;
use App\Entity\Logement;

class LogementDTO {
    private string $name;
    private float $priceByMonth;
    private $addressId = null;

    /**
     * @param Logement $logement
     * @return LogementDTO
     */
    public static function fromLogement(Logement $logement) {
        $dto = new LogementDTO();

        $dto
            ->setName($logement->getName())
            ->setPriceByMonth($logement->getPriceByMonth());
        if ($logement->getAddress() != null) {
            $dto->setAddress($logement->getAddress());
        }

        return $dto;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return LogementDTO
     */
    public function setName(string $name): LogementDTO
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceByMonth(): float
    {
        return $this->priceByMonth;
    }

    /**
     * @param float $priceByMonth
     * @return LogementDTO
     */
    public function setPriceByMonth(float $priceByMonth): LogementDTO
    {
        $this->priceByMonth = $priceByMonth;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAddressId()
    {
        return $this->addressId;
    }

    /**
     * @param int $addressId
     * @return LogementDTO
     */
    public function setAddress(Address $address): LogementDTO
    {
        $this->addressId = $address->getId();
        return $this;
    }


}