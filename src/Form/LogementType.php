<?php

namespace App\Form;

use App\Models\Forms\AddressCreateForm;
use App\Models\Forms\LogementForm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LogementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('logement', LogementCreateType::class)
            ->add('address', AddressCreateType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
            "data_class" => LogementForm::class,
            "csrf_protection" => false
        ]);
    }
}
