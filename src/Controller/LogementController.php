<?php

namespace App\Controller;

use App\Entity\Logement;
use App\Form\AddressCreateType;
use App\Form\LogementCreateType;
use App\Form\LogementType;
use App\Models\Forms\AddressCreateForm;
use App\Models\Forms\LogementCreateForm;
use App\Models\Forms\LogementForm;
use App\Models\LogementDTO;
use App\Repository\AddressRepository;
use App\Repository\LogementRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LogementController
 * @package App\Controller
 * @Route(path="/api/logement")
 */
class LogementController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/", name="api_logement_readAll")
     * @Rest\View()
     * @param LogementRepository $repository
     * @return View
     */
    public function readAllAction(LogementRepository $repository) {
        return $this->view(["logements" => $repository->findAll()], 200);
    }

    /**
     * @Rest\Get(path="/{id}", name="api_logement_readById", requirements={"id": "\d+"})
     * @Rest\View()
     * @param Logement $logement
     * @return View
     * @ParamConverter("logement", class="App\Entity\Logement")
     */
    public function readOneAction(Logement $logement) {
        if ($logement != null) {
            return $this->view(["logement" => LogementDTO::fromLogement($logement)], Response::HTTP_OK);
        } else {
            return $this->view(["errors" => ["id not found"]], Response::HTTP_PRECONDITION_FAILED);
        }
    }

    /**
     * @Rest\Get(path="/address", name="api_logement_readAddresses")
     * @Rest\View()
     */
    public function readAllAddress(AddressRepository $repository) {
        return $this->view(["addresses" => $repository->findAllByLogement()]);
    }
    /**
     * @Rest\Get(path="/{id}/address", name="api_logement_readOneAddress")
     * @Rest\View()
     * @param Logement $logement
     * @return View
     * @IsGranted("ROLE_ADMIN")
     */
    public function readAddressByLogementIdAction(Logement $logement) {

        return $this->view(["address" => $logement->getAddress()], Response::HTTP_OK);
    }

    /**
     * @Rest\Post(path="/", name="api_logement_create")
     * @Rest\View()
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return
     * @IsGranted("ROLE_USER")
     */
    public function createAction(Request $request, EntityManagerInterface $em) {
        $create = new LogementCreateForm();
        $data = json_decode($request->getContent(), true);
        $form = $this->createForm(LogementCreateType::class, $create);
        $form->submit($data);

        if ($form->isValid()) {
            $em->persist(LogementCreateForm::toLogement($create));
            $em->flush();
            return $this->view($create, Response::HTTP_CREATED);
        }

        return $this->view($form->getErrors(), Response::HTTP_PRECONDITION_FAILED);
    }

    /**
     * @Rest\Post(path="/address", name="api_logement_createWithAddress")
     * @Rest\View()
     * @param Request $request
     */
    public function createWithAddressAction(Request $request, EntityManagerInterface $em) {
        $formType = new LogementForm();

        $form = $this->createForm(LogementType::class, $formType);
        $data = json_decode($request->getContent(), true);

        $form->submit($data);
        if ($form->isValid()) {
            $logement = LogementForm::toLogement($formType);
            $em->persist($logement);
            $em->flush();

            return $this->view($logement, Response::HTTP_CREATED);
        }

        return $this->view(["errors" => $form->getErrors()], Response::HTTP_PRECONDITION_FAILED);
    }

    /**
     * @Rest\Patch(path="/{id}/address")
     * @Rest\View()
     * @param Logement $logement
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function addAddressAction(Logement $logement, Request $request, EntityManagerInterface $em) {
        $addressCreateForm = new AddressCreateForm();

        $form = $this->createForm(AddressCreateType::class, $addressCreateForm);
        $form->submit(json_decode($request->getContent(), true));

        if ($form->isValid()) {
            $logement->setAddress(AddressCreateForm::toAddress($addressCreateForm));
            $em->flush();
            return ["logement" => $logement];
        }

        return ["errors" => $form->getErrors()];
    }
}
