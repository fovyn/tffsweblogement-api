<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractFOSRestController
{
    /**
     * @Rest\Post(path="/api/login_check", name="api_login")
     */
    public function loginCheckAction() {
        $user = $this->getUser();

        return new JsonResponse(["username" => $user->getUsername(), "roles" => $user->getRoles()]);
    }

    /**
     * @Rest\Get(path="/api/security/roles", name="api_security_roles")
     * @Rest\View()
     * @IsGranted("ROLE_USER")
     */
    public function rolesAction() {
        $user = $this->getUser();

        return ["roles" => $user->getRoles()];
    }
}
