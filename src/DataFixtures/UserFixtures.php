<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\UserPassportInterface;

class UserFixtures extends Fixture
{
    //PasswordEncoder => hash le mots de passe
    private UserPasswordEncoderInterface $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setUsername("admin");
        $admin->setPassword($this->encoder->encodePassword($admin, "admin"));

        $roleAdmin = new Role();
        $roleAdmin->setLabel("ROLE_ADMIN");
        $roleUser = new Role();
        $roleUser->setLabel("ROLE_USER");
        $roleCreateRole = new Role();
        $roleCreateRole->setLabel("ROLE_ROLE_CREATOR");

        $groupAdmin = new Group();
        $groupAdmin->setLabel("GROUP_ADMIN");
        $groupAdmin->addRole($roleAdmin)->addRole($roleCreateRole);

        $groupUser = new Group();
        $groupUser->setLabel("GROUP_USER");
        $groupUser->addRole($roleUser);


        $admin->addGroup($groupAdmin)
            ->addGroup($groupUser);
        // $product = new Product();
        // $manager->persist($product);

        $manager->persist($admin);
        $manager->flush();
    }
}
