<?php

namespace App\DataFixtures;

use App\Entity\Logement;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LogementFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $logement1 = new Logement();

        $logement1
            ->setName("Logement 1")
            ->setPriceByMonth(800);

        $manager->persist($logement1);
        $manager->flush();
    }
}
